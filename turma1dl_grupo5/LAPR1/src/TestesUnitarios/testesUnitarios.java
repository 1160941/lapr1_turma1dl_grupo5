package TestesUnitarios;

import java.io.FileNotFoundException;
import java.util.Arrays;
import lapr1.AHP;
import static lapr1.Main.*;
import static lapr1.TOPSIS.*;
import static lapr1.Util.*;
import static lapr1.AHP.*;
import lapr1.Util;

public class testesUnitarios {

    private static final int ORDEM_MAT_1 = 3;

    public static void main(String[] args) throws FileNotFoundException {
        double mat[][] = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        double[][] matIR = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        System.out.print("Teste relativo ao metodo calcIR: ");
        System.out.println(teste_calcIR(matIR, 0.58));

        System.out.print("Teste relativo ao metodo criarVetorSomaCol: ");
        double[] vecEsperado = {12, 15, 18};
        System.out.println(teste_criarVetorSomaCol(mat, vecEsperado));

        System.out.print("Teste relativo ao metodo dividir: ");
        System.out.println(teste_dividir(1, 2));

        System.out.print("Teste relativo ao metodo criarVetorProprioAproximado: ");
        double[] vecEsperado2 = {0.12777777777777777, 0.3333333333333333, 0.5388888888888889
        };
        System.out.println(teste_criarVetorProprioAproximado(mat, vecEsperado2));

        System.out.print("Teste relativo ao metodo calcValorProprioMaximoAproximado: ");

        System.out.println(teste_calcValorProprioMaximoAproximado(mat, mat.length));

        System.out.print("Teste relativo ao metodo multiplicar Arrays: ");
        double[][] identidade = {{1, 0}, {0, 1}};
        double[] vecDois = {2, 2};
        System.out.println(teste_multiplicarArrays(identidade, vecDois));

        System.out.print("Teste relativo ao metodo determinarOpIdeal: ");
        double[] vecTeste = {5, 4, 1};
        double[] vecEsperado3 = {1, 5};
        System.out.println(teste_determinarOpIdeal(vecTeste, vecEsperado3));

        System.out.print("Teste relativo ao metodo calcExactEigenVector: ");

        double[][] matTeste = {{2, 4}, {3, 13}};
        double[] vecEsperado4 = {1, 3};

        System.out.println(teste_calcExactEigenVector(matTeste, vecEsperado4));

        System.out.print("Teste relativo ao metodo determinarMaiorElemDiagonal: ");
        double[][] matTeste1 = {{7, 0}, {0, -1}};
        int indEsp = 0;
        System.out.println(teste_determinarMaiorElemDiagonal(matTeste1, indEsp));

        System.out.print("Teste relativo ao metodo findMax: ");
        double maxEsperado = 9;

        System.out.println(teste_findMax(mat, maxEsperado));

        System.out.print("Teste relativo ao metodo calcRC: ");
        double rcEsperado = 11.307586206896554;
        double maxEigenValue = 16.1168;
        System.out.println(teste_calcRC(mat, maxEigenValue, rcEsperado));
        System.out.print("Teste relativo ao metodo elevarElementosMatAoQuadrado: ");
        double[][] matElementosAoQuadradoEsperada = {{1, 4, 9}, {16, 25, 36}, {49, 64, 81}};
        double[][] mataoQuadrado = new double[3][3];
        System.out.println(teste_elevarElementosMatAoQuadrado(mat, mataoQuadrado, matElementosAoQuadradoEsperada));
        double[] SomaRQdosQuadradosEsperado = {8.124038405, 9.643650761, 11.22497216};
        double[] SomaRQdosQuadrados = new double[3];
        System.out.print("Teste relativo ao método criarVetorComSomadosElementosDeColunaaoQuadrado: ");
        System.out.println(teste_criarVetorComSomadosElementosDeColunaaoQuadrado(mat, SomaRQdosQuadrados, SomaRQdosQuadradosEsperado));
        System.out.print("Teste relativo ao método teste_construirMatrizNormalizada (TOPSIS): ");
        double[][] matNormalizadaEsperada = {{0.123, 0.246, 0.369}, {0.415, 0.518, 0.622}, {0.624, 0.7137, 0.802}};
        System.out.println(teste_construirMatrizNormalizadaTOPSIS(mat, SomaRQdosQuadrados, matNormalizadaEsperada));
        System.out.print("Teste relativo ao método contruirMatrizNormailizadaPesada (TOPSIS): ");
        double[] pesos = {0.2, 0.2, 0.6};
        double[][] matNormalizadaPesadaEsperada = {{0.0246, 0.0492, 0.2214}, {0.083, 0.1036, 0.3732}, {0.1248, 0.14274, 0.4812}};
        System.out.println(teste_contruirMatrizNormailizadaPesadaTOPSIS(matNormalizadaEsperada, pesos, matNormalizadaPesadaEsperada));
        System.out.print("Teste relativo ao método vetorSolucoesIdeais (TOPSIS): ");
        double[] solucoesIdeais = new double[3];
        String[] maioresoumenores = {"+", "+", "+"};
        double[] solucoesIdeaisEsperadas = {0.1248, 0.14274, 0.4812};
        System.out.println(teste_vetorSolucoesIdeaisTOPSIS(matNormalizadaEsperada, solucoesIdeais, maioresoumenores, solucoesIdeaisEsperadas));
        System.out.print("Teste relativo ao método teste_VetorSeparacaodaSolucaoIdeal (TOPSIS): ");
        double[] ditSolucaoIdeal = new double[3];
        double[] ditSolucaoIdealEsperada = {0.294, 0.122, 0};
        System.out.println(teste_VetorSeparacaodaSolucaoIdealTOPSIS(matNormalizadaPesadaEsperada, solucoesIdeaisEsperadas, ditSolucaoIdeal, ditSolucaoIdealEsperada));
        System.out.print("Teste relativo ao método teste_VetoRSolucaoMaisPertodaIdeal (TOPSIS): ");
        double[] solucoesMaisPertodaIdeal = new double[4];
        double[] ditSolucaoIdealTESTE = {0.029, 0.057, 0.09, 0.058};
        double[] ditSolucaoIdealNegativaTESTE = {0.083, 0.040, 0.019, 0.047};
        double[] solucoesMaisPertodaIdealEsperadas = {0.74, 0.41, 0.17, 0.45};
        System.out.println(teste_VetorSolucaoMaisPertodaIdealTOPSIS(ditSolucaoIdealTESTE, ditSolucaoIdealNegativaTESTE, solucoesMaisPertodaIdeal, solucoesMaisPertodaIdealEsperadas));
        System.out.print("Teste relativo ao método CalculoNeM: ");
        int[] vecEspAHP = {3, 4};
        int[] vecEspTOPSIS = {4, 4};
        String opCLIAHP = "1";
        String opCLITOPSIS = "2";
        System.out.println(teste_calculoAltCrit(opCLIAHP, opCLITOPSIS, "inputAHPteste.txt", "inputTOPSISteste.txt", vecEspAHP, vecEspTOPSIS));

        System.out.print("Teste relativo ao metodo criarVecRC: ");
        double mat3D[][][] = {{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}, {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}};
        double[] vecRCEsperado = {11.985961486610648,11.985961486610648};
        
        System.out.println(teste_criarVecRC(mat3D, vecRCEsperado));

        //Teste ifMatCons
        double[][][] mat3D2 = {{{1, 0.5, 3}, {2, 1, 4}, {0.33333, 0.25, 1}}};
        double[] rcVec2 = {0.01580};
        double[] lambdaVec2 = {3.01832};
        boolean eConsistenteEsperado = true;
        System.out.print("Teste relativo ao método ifMatCons: ");
        System.out.println(test_ifMatCons(mat3D2, rcVec2, lambdaVec2, eConsistenteEsperado));

        //Teste construirMatrizNormalizada
        double[][] mat2 = {{1, 0.5, 3}, {2, 1, 4}, {0.33333, 0.25, 1}};
        double[] soma = {3.33333, 1.75, 8};
        double[][] matNormEsperada = {{0.3000003000003, 0.2857142857142857, 0.375}, {0.6000006000006, 0.5714285714285714, 0.5}, {0.0999990999991, 0.14285714285714285, 0.125}};
        System.out.print("Teste relativo ao método construirMatrizNormalizada: ");
        System.out.println(test_construirMatrizNormalizada(mat2, soma, matNormEsperada));

        //Teste normMat3d
        double[][][] mat3d = {{{1, 0.5, 3}, {2, 1, 4}, {0.3333333333, 0.25, 1}},
        {{1, 0.25, 0.2, 0.1666666667}, {4, 1, 0.3333333333, 0.14286}, {5, 3, 1, 0.5}, {6, 7, 2, 1}},
        {{1, 3, 9, 3}, {0.3333333333, 1, 7, 1}, {0.1111111111, 0.14286, 1, 0.14286}, {0.3333333333, 1, 7, 1}},
        {{1, 4, 5, 6}, {0.25, 1, 3, 7}, {0.2, 0.3333333333, 1, 2}, {0.1666666667, 0.14286, 0.5, 1}}};
        int[] vecCritAlt = {3, 4};
        double[][][] mat3dNormEsperada = {{{0.300000000003, 0.2857142857142857, 0.375}, {0.600000000006, 0.5714285714285714, 0.5}, {0.09999999999099998, 0.14285714285714285, 0.125}},
        {{0.0625, 0.022222222222222223, 0.056603773585439665, 0.09210511774548583}, {0.25, 0.08888888888888889, 0.09433962263296547, 0.07894882271093086}, {0.3125, 0.26666666666666666, 0.2830188679271983, 0.27631535318119443}, {0.375, 0.6222222222222222, 0.5660377358543967, 0.5526307063623889}},
        {{0.5625000000246094, 0.5833330092594393, 0.375, 0.5833330092594393}, {0.18749999998945313, 0.1944443364198131, 0.2916666666666667, 0.1944443364198131}, {0.06249999999648438, 0.027778317900934497, 0.041666666666666664, 0.027778317900934497}, {0.18749999998945313, 0.1944443364198131, 0.2916666666666667, 0.1944443364198131}},
        {{0.6185567010181741, 0.7304344015169324, 0.5263157894736842, 0.375}, {0.15463917525454351, 0.1826086003792331, 0.3157894736842105, 0.4375}, {0.12371134020363482, 0.06086953345365741, 0.10526315789473684, 0.125}, {0.10309278352364756, 0.026087464650177236, 0.05263157894736842, 0.0625}}};
        System.out.print("Teste relativo ao método normMat3d: ");
        System.out.println(test_normMat3d(mat3d, vecCritAlt, mat3dNormEsperada));

        //Teste procuraLimiarPesos
        double[] vecPesos = {0.32024, 0.55714, 0.12262};
        double limiarImportancia = 0;
        int posEsperada = -1;
        System.out.print("Teste relativo ao método procuraLimiarPesos: ");
        System.out.println(test_procuraLimiarPesos(vecPesos, limiarImportancia, posEsperada));

        //Teste removLinhaColdeMatrix
        double[][] matrix = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int pos1 = 1;
        double[][] newMatEsperada = {{1, 3}, {7, 9}};
        System.out.print("Teste relativo ao método removLinhaColdeMatrix: ");
        System.out.println(test_removLinhaColdeMatrix(matrix, pos1, newMatEsperada));

        //Teste removeMatrix
        double[][][] matrix3D = {{{1, 2,3}, {3, 4,5}}, {{5, 6}, {7, 8}}, {{9, 1}, {2, 3}}};
        double [][][]mat3DEsperada={{{1, 2}, {3, 4}}, {{9, 1}, {2, 3}}};
        int pos2 = 2;
        int nAlts = 2;
        int nCrit = 2;
        System.out.print("Teste relativo ao método removeMatrix: ");
        System.out.println(teste_removeMatrix(matrix3D, mat3DEsperada, pos2, nCrit, nAlts));

        //Teste preenchVetIR
        double[] irVec = new double[2];
        double[][][] mat3d2 = {{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}, {{9, 8, 7}, {6, 5, 4}, {3, 2, 1}}};
        double[] irVecEsperado = {0.58, 0.58};
        System.out.print("Teste relativo ao método preenchVetIR: ");
        System.out.println(test_preenchVetIR(irVec, mat3d2, irVecEsperado));
    }

    public static boolean teste_calculoAltCrit(String opCLIAHP, String opCLITOPSIS, String nomeficheiroAHP, String nomeficheiroTOPSIS, int[] vecEspAHP, int[] vecEspTOPSIS) throws FileNotFoundException {
        boolean flag = true;
        int[] vec = new int[2];
        vec = calculoCritAlt(opCLIAHP, nomeficheiroAHP);
        for (int i = 0; i < 2; i++) {
            if (vec[i] != (vecEspAHP[i])) {
                flag = !flag;
            }
        }
        vec = calculoCritAlt(opCLITOPSIS, nomeficheiroTOPSIS);
        for (int i = 0; i < 2; i++) {
            if (vec[i] != (vecEspTOPSIS[i])) {
                flag = false;
            }
        }
        return flag;
    }

    public static boolean teste_calcIR(double[][] mc_criterios, double valorEsperado) {
        return calcIR(mc_criterios) == valorEsperado;
    }

    public static boolean teste_criarVetorSomaCol(double[][] mat, int ordem) {
        System.out.print("Teste associado ao metodo criarVetorSomaCol: ");
        double vecSomaEsperado[] = {12.0, 15.0, 18.0};
        String resp1 = Arrays.toString(criarVetorSomaCol(mat, ORDEM_MAT_1));
        String resp2 = Arrays.toString(vecSomaEsperado);
        if (resp1.equals(resp2)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean teste_normalizar(double[] vecSoma, double[][] mat, int ordem, double matNormalEsperada[][]) {
        System.out.println("Teste associado ao metodo normalizarMatriz");
        mat = normalizarMatriz(vecSoma, mat, ORDEM_MAT_1);

        return Arrays.deepEquals(mat, matNormalEsperada);
    }

    public static boolean testarCriarVetorProprioAproximado(double[][] mat, int ordem) {
        System.out.println("Teste associado ao metodo criarVetorProprioAproximado");
        double vetorProprioEsperado[] = {2.0, 5.0, 8.0};
        String resp1 = Arrays.toString(criarVetorSomaCol(mat, ORDEM_MAT_1));
        String resp2 = Arrays.toString(vetorProprioEsperado);
        if (resp1.equals(resp2)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean teste_criarVetorSomaLin(double[][] matAux, int ordem) {
        System.out.println("Teste associado ao metodo criarVetorSomaLin");
        double multiResultEsperado[] = {36.0, 81.0, 126.0};
        String resp1 = Arrays.toString(criarVetorSomaLin(matAux, ORDEM_MAT_1));
        String resp2 = Arrays.toString(multiResultEsperado);
        return (resp1.equals(resp2));
    }

    public static boolean teste_calcValorProprioMaximoAproximado(double[][] mat, int ordem) {
        double valorProprioEsperado = 16.90371532446835;
        String resp1 = Double.toString(calcValorProprioMaximoAproximado(mat));
        String resp2 = Double.toString((valorProprioEsperado));
        return resp1.equals(resp2);
    }

    public static boolean teste_verOp(String inputTeste1, String valEsperado1, String valEsperado2) {
        String op = verOp(inputTeste1);
        return (op.equals(valEsperado1) || op.equals(valEsperado2));
    }

    public static boolean teste_criarVetorSomaCol(double[][] mat, double[] vecEsperado) {
        double[] vecObtido = criarVetorSomaCol(mat, ORDEM_MAT_1);
        return Arrays.equals(vecObtido, vecEsperado);
    }

    public static boolean teste_dividir(double inputNumerador, double inputDenominador) {
        return dividir(inputNumerador, inputDenominador) == inputNumerador / inputDenominador;
    }

    public static boolean teste_criarVetorProprioAproximado(double[][] mat, double[] vecEsperado) {
        double[] vecObtido = criarVetorProprioAproximado(mat);
        return Arrays.equals(vecObtido, vecEsperado);
    }

    public static boolean teste_determinarOpIdeal(double[] vec, double[] vecEsperado) {
        double[] vecObtido = determinarOpIdeal(vec);
        return Arrays.equals(vecObtido, vecEsperado);
    }

    public static boolean teste_calcExactEigenVector(double[][] mat, double[] vecEsperado) {
        double[] vecObtido = calcExactEigenVector(mat);

        return Math.round(vecEsperado[0] / vecEsperado[1]) == Math.round(vecObtido[0] / vecObtido[1]);

    }

    public static boolean teste_determinarMaiorElemDiagonal(double[][] mat, int indEsp) {
        int matIndOb = determinarMaiorElemDiagonal(mat);
        return (matIndOb == indEsp);
    }

    public static boolean teste_findMax(double[][] mat, double maxEsperado) {
        double max = findMax(mat);
        return max == maxEsperado;
    }

    public static boolean teste_calcRC(double[][] mat, double maiorValorProprio, double RCEsperado) {
        return calcRC(mat, maiorValorProprio) == RCEsperado;
    }

    public static boolean teste_transposeMatrix(double[][] mat, double[][] matTranspostaEsperada) {
        double[][] matTransposta = transposeMatrix(mat);
        return Arrays.deepEquals(matTransposta, matTranspostaEsperada);
    }

    public static boolean teste_elevarElementosMatAoQuadrado(double[][] mat, double[][] matAoQuadrado, double[][] MatAoQuadradoEsperada) {
        elevarElementosMatAoQuadrado(mat, matAoQuadrado);
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[0].length; j++) {
                if (matAoQuadrado[i][j] != MatAoQuadradoEsperada[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean teste_criarVetorComSomadosElementosDeColunaaoQuadrado(double[][] matAoQuadrado, double[] SomaRQdosQuadrados, double[] SomaRQdosQuadradosEsperado) {
        SomaRQdosQuadrados = criarVetorComSomadosElementosDeColunaaoQuadrado(matAoQuadrado);
        for (int i = 0; i < SomaRQdosQuadrados.length; i++) {
            if (Math.round(SomaRQdosQuadrados[i]) != Math.round(SomaRQdosQuadradosEsperado[i])) {
                return false;
            }
        }
        return true;
    }

    public static boolean teste_construirMatrizNormalizadaTOPSIS(double[][] mat, double[] SomaRQdosQuadrados, double[][] matnormalizadaesperada) {
        normalizarMatriz(mat);
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat.length; j++) {
                if (Math.round(mat[i][j]) != Math.round(matnormalizadaesperada[i][j])) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean teste_contruirMatrizNormailizadaPesadaTOPSIS(double[][] matNormalizada, double[] pesos, double[][] matNormalizadaPesadaEsperada) {
        contruirMatrizNormailizadaPesada(matNormalizada, pesos);
        for (int i = 0; i < matNormalizada.length; i++) {
            for (int j = 0; j < matNormalizada.length; j++) {
                if (Math.round(matNormalizada[i][j]) != Math.round(matNormalizadaPesadaEsperada[i][j])) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean teste_vetorSolucoesIdeaisTOPSIS(double[][] matNormalizadaPesada, double[] solucoesIdeais, String[] maioresoumenores, double[] solucoesIdeaisEsperadas) {
        vetorSolucoesIdeais(matNormalizadaPesada, solucoesIdeais, maioresoumenores);
        for (int i = 0; i < matNormalizadaPesada.length; i++) {
            if (Math.round(solucoesIdeais[i]) != Math.round(solucoesIdeaisEsperadas[i])) {
                return false;
            }
        }
        return true;
    }

    public static boolean teste_VetorSeparacaodaSolucaoIdealTOPSIS(double[][] matNormalizadaPesada, double[] solucoesIdeais, double[] ditSolucaoIdeal, double[] ditSolucaoIdealEsperada) {
        vetorSeparacaodaSolucaoIdeal(matNormalizadaPesada, solucoesIdeais, ditSolucaoIdeal);
        for (int i = 0; i < matNormalizadaPesada.length; i++) {
            if (Math.round(ditSolucaoIdeal[i]) != Math.round(ditSolucaoIdealEsperada[i])) {
                return false;
            }
        }
        return true;
    }

    public static boolean teste_VetorSolucaoMaisPertodaIdealTOPSIS(double[] ditSolucaoIdeal, double[] ditSolucaoIdealNegativa, double[] solucoesMaisPertodaIdeal, double[] solucoesMaisPertodaIdealEsperadas) {
        VetorSolucaoMaisPertodaIdeal(ditSolucaoIdeal, ditSolucaoIdealNegativa, solucoesMaisPertodaIdeal);
        for (int i = 0; i < ditSolucaoIdeal.length; i++) {
            if (Math.round(solucoesMaisPertodaIdeal[i]) != Math.round(solucoesMaisPertodaIdealEsperadas[i])) {
                return false;
            }
        }
        return true;
    }

    public static boolean teste_criarVecRC(double[][][] mat3D, double[] vecRCesperado) throws FileNotFoundException {
        return Arrays.equals(criarVecRC(mat3D), vecRCesperado);
    }

    public static boolean teste_multVecPrioridadeComposta(double[][] mat, double[] vecPesos, double[] opVecEsperado) {
        double[] opVec = Util.multVecPrioridadeComposta(mat, vecPesos);
        for (int i = 0; i < opVec.length; i++) {
            if (opVec[i] != opVecEsperado[i]) {
                return false;
            }
        }
        return true;
    }

    public static boolean test_preenchVetIR(double[] irVec, double[][][] mat3d, double[] irVecEsperado) {
        preenchVetIR(irVec, mat3d);
        for (int i = 0; i < irVec.length; i++) {
            if (irVec[i] != irVecEsperado[i]) {
                return false;
            }
        }
        return true;
    }

    public static boolean test_removeMatrix(double[][][] matrix3D, double[][][] matrix3DEsperada, int pos, int nCrit, int nAlts) {
        Util.removeMatrix(matrix3D, pos, nCrit, nAlts);
        return Arrays.deepEquals(matrix3D, matrix3DEsperada);
    }

    public static boolean test_removLinhaColdeMatrix(double[][] matrix, int pos, double[][] newMatEsperada) {
        double[][] newMat = removLinhaColdeMatrix(matrix, pos);
        for (int l = 0; l < newMat.length; l++) {
            for (int c = 0; c < newMat[l].length; c++) {
                if (newMat[l][c] != newMatEsperada[l][c]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean test_procuraLimiarPesos(double[] vecPesos, double limiarImportancia, int posEsperada) {
        int pos = procuraLimiarPesos(vecPesos, limiarImportancia);
        return (pos == posEsperada);
    }

    public static boolean test_normMat3d(double[][][] mat3d, int[] vecCritAlt, double[][][] mat3dNormEsperada) {
        double[][][] mat3dNorm = normMat3d(mat3d, vecCritAlt);
        for (int i = 0; i < mat3dNorm.length; i++) {
            for (int j = 0; j < mat3dNorm[i].length; j++) {
                for (int k = 0; k < mat3dNorm[i][j].length; k++) {
                    if (mat3dNorm[i][j][k] != mat3dNormEsperada[i][j][k]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public static boolean test_construirMatrizNormalizada(double[][] mat, double[] soma, double[][] matNormEsperada) {
        double[][] matNorm = AHP.construirMatrizNormalizadaAHP(mat, soma);
        for (int l = 0; l < matNorm.length; l++) {
            for (int c = 0; c < matNorm[l].length; c++) {
                if (matNorm[l][c] != matNormEsperada[l][c]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean test_ifMatCons(double[][][] mat3D, double[] rcVec, double[] lambdaVec, boolean eConsistenteEsperado) throws FileNotFoundException {
        boolean eConsistente = ifMatCons(mat3D, rcVec, lambdaVec);
        return (eConsistenteEsperado == eConsistente);
    }

    private static boolean teste_multiplicarArrays(double[][] identidade, double[] vec) {
        return Arrays.equals(multiplicarArrays(identidade, vec), vec);
    }

    private static boolean teste_removeMatrix(double[][][] matrix3D, double[][][] matrix3DEsperada, int pos2, int nCrit, int nAlts) {
        return Arrays.deepEquals(matrix3DEsperada=(removeMatrix(matrix3D, pos2, nCrit, nAlts)), matrix3DEsperada);
    }

}
