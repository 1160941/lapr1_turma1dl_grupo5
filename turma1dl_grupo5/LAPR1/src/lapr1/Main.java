/**
 * Classe principal
 */
package lapr1;

import static lapr1.AHP.*;
import static lapr1.Util.*;
import static lapr1.TOPSIS.*;
import static lapr1.List.*;

import java.util.*;

import java.io.*;

public class Main {

    static Scanner sc = new Scanner(System.in);
    static double limiarConsistencia = 0.1;
    static double limiarImportancia = 0;
    private final static String FICH_LOG_ERROS = "LogErros" + getDayMonthYearHour() + ".txt";

    public static void main(String[] args) throws FileNotFoundException {
        String nomeFicheiro = null;
        String nomeFicheiroOutput = null;
        String data = getDayMonthYearHour();
        if (args.length >= 2) {
            nomeFicheiro = args[args.length - 2];
            nomeFicheiroOutput = args[args.length - 1];
        } else {
            System.out.println("Adicione os nomes dos ficheiros de input e output.");
            System.exit(255);
        }

        Formatter writeFile = new Formatter(new File(nomeFicheiroOutput));
        Formatter cons = new Formatter(System.out);
        Formatter logErros = LogErros.criarEAbrirFileLogErros(FICH_LOG_ERROS);
        String opCLI = controlaOpcoesCLI(args, writeFile, nomeFicheiro);
        int[] vecCritAlt = calculoCritAlt(opCLI, nomeFicheiro); 
        //metodo AHP
        if (opCLI.equals("1")) {
            //FAZ AHP
            String[] nomeCrits = new String[vecCritAlt[0]];
            String[] nomeAlts = new String[vecCritAlt[1]]; 
            System.out.println("No caso de existir algum erro na leitura do ficheiro, por favor visite:" + FICH_LOG_ERROS);
            double[][][] mat3D = AHP.lerFicheiroMatrizesAHP(nomeCrits, nomeAlts, nomeFicheiro, vecCritAlt,logErros);
            System.out.println("\nEste problema foi resolvido recorrendo ao método AHP.\n");
            //mostrar de comparação de entrada no ficheiro e na consola:
            writeFile.format("%n", "Este problema foi resolvido segundo o método AHP");
            List.listarMats("A matrizes de entrada e de comparação são ", mat3D, vecCritAlt, cons, nomeCrits);
            List.listarMats("A matrizes de entrada e de comparação são ", mat3D, vecCritAlt, writeFile, nomeCrits);

            //Normalizar matrizes :
            double[][][] mat3DNorm = AHP.normMat3d(mat3D, vecCritAlt);
            List.listarMats("As matrizes normalizadas são:", mat3DNorm, vecCritAlt, writeFile, nomeCrits);
            double[] irVec = new double[vecCritAlt[0] + 1];
            preenchVetIR(irVec, mat3D);//PREENCHE UM VETOR GLOBAL COM OS VALORES DO IR
            double[] rcVec = new double[vecCritAlt[0] + 1];
            double[] lambdaVec = new double[vecCritAlt[0] + 1];

            //declarar matriz com os vetores de prioridade composta
            double[][] matPriorComp = new double[vecCritAlt[0]][];
            double[] vecPesos;
            for (int i = 0; i < matPriorComp.length; i++) {
                matPriorComp[i] = new double[vecCritAlt[1]];
            }
            if (AHP.ifMatCons(mat3D, rcVec, lambdaVec)) {

                //Dar a opcao para exato ou aproximado
                String op = menu();
                if (op.equals("1")) {//calculo aproximado
                    listarLambdaIRRCAproximados(writeFile,mat3D, nomeCrits);
                    vecPesos = criarVetorProprioAproximado(mat3D[0]);
                    for (int i = 0; i < mat3D.length - 1; i++) { //calcular o vetor da prioridade composta
                        matPriorComp[i] = criarVetorProprioAproximado(mat3D[i + 1]);
                    }

                } else {//calculo exato

                    //mostrar IR,RC e lambda maximo:
                    listarLambdaMaxIRRCExato(writeFile,rcVec, irVec, lambdaVec, nomeCrits, vecCritAlt);
                    vecPesos = calcExactEigenVector(mat3D[0]);
                    for (int i = 0; i < mat3D.length - 1; i++) { //calcular o vetor da prioridade composta
                        matPriorComp[i] = calcExactEigenVector(mat3D[i + 1]);
                    }

                }//Ver se passou o limiar em argumento 
                if (limiarImportancia != 0) {
//Então é necessário ver o limiar de importância
                    int pos = procuraLimiarPesos(vecPesos, limiarImportancia);
                    if (pos == -1) {//Não existem valores inferiores ao limiar
                        System.out.println("\nNão existem valores inferiores ao limiar, o programa vai prosseguir.");
                        System.out.println("");
                    } else {
                        System.out.printf("\nEncontrou-se valores inferiores ao limiar.");
                        do {
                            pos = procuraLimiarPesos(vecPesos, limiarImportancia);
                            if (pos >= 0) {
                                vecCritAlt[0] = vecCritAlt[0] - 1;
                                for (int i = pos; i < nomeCrits.length-1; i++) {
                                  nomeCrits[i]=nomeCrits[i+1];  
                                }
                                
                                mat3D = removeMatrix(mat3D, pos + 1, vecCritAlt[0], vecCritAlt[1]);
                                mat3D[0] = removLinhaColdeMatrix(mat3D[0], pos);
                            }
                            vecPesos = calcExactEigenVector(mat3D[0]);
                        } while (pos != -1);

                    }
                }
                double IRMatCrit = Util.calcIR(mat3D[0]);
                if (IRMatCrit == 0.0) { //Verificar se a ordem da matriz continua válida
                    LogErros.registarErro(logErros, "IR da matriz critérios " + IRMatCrit, "Ordem da matriz não suportada, o programa vai terminar.");
                    System.out.println("\nIR da matriz critérios " + IRMatCrit + " \nOrdem da matriz não suportada, o programa vai terminar.");
                    logErros.close();
                    System.exit(1);
                } else { //A consistência ainda é válida
                    //Normalizar matrizes novamente e mostra-las:
                    mat3DNorm = AHP.normMat3d(mat3D, vecCritAlt);
                    List.listarMats("As matrizes normalizadas depois de remover os valores são:", mat3DNorm, vecCritAlt, writeFile, nomeCrits);
                    matPriorComp = new double[vecCritAlt[0]][];
                    for (int i = 0; i < matPriorComp.length; i++) {
                        matPriorComp[i] = new double[vecCritAlt[1]];
                    }

                    if (op.equals("1")) {//calculo aproximado
                        listarLambdaIRRCAproximados(writeFile,mat3D, nomeCrits);
                        vecPesos = criarVetorProprioAproximado(mat3D[0]);
                        for (int i = 1; i < mat3D.length; i++) { //calcular o vetor da prioridade composta
                            matPriorComp[i - 1] = criarVetorProprioAproximado(mat3D[i]);
                        }
                    } else {//calculo exato

                        //mostrar IR,RC e lambda maximo:
                        listarLambdaMaxIRRCExato(writeFile,rcVec, irVec, lambdaVec, nomeCrits, vecCritAlt);
                        vecPesos = calcExactEigenVector(mat3D[0]);
                        for (int i = 1; i < mat3D.length; i++) { //calcular o vetor da prioridade composta
                            matPriorComp[i - 1] = calcExactEigenVector(mat3D[i]);
                        }
                    }
                }
                matPriorComp = transposeMatrix(matPriorComp);
                double[] vecPriorComposta = multVecPrioridadeComposta(matPriorComp, vecPesos);
                listarMatNormVetComp(writeFile,"\nAs matrizes normalizadas com o respetivo vetor de prioridades é:\n", mat3DNorm, vecPesos, matPriorComp, nomeCrits);
                listarVec("Vetor de prioridade composta", cons, vecPriorComposta);
                listarVec("Vetor de prioridade composta", writeFile, vecPriorComposta);
                printOpcaoAdequada("A opção mais adequada é ", vecPriorComposta, cons);
                printOpcaoAdequada("A opção mais adequada é ", vecPriorComposta, writeFile);
                writeFile.format("ISEP Instituto Superior de Engenharia do Porto " + data + ".");
                System.out.println("ISEP Instituto Superior de Engenharia do Porto " + data + ".");

            } else {
                System.out.println("As matrizes não são consistentes, o programa vai terminar.");
                System.out.println("ISEP Instituto Superior de Engenharia do Porto " + data + ".");
                LogErros.registarErro(logErros, "Verificação da consistência das matrizes: falso", "As matrizes não são consistentes");
                System.exit(1);
            }
        } else if (opCLI.equals("2")) {
            System.out.println("No caso de existir algum erro na leitura do ficheiro, por favor visite:" + FICH_LOG_ERROS);
            //Cria uma matriz com base no numero de alternativas e criterios
            double[][] matrizTOPSIS = new double[vecCritAlt[1]][vecCritAlt[0]];
            //Cria os vetores pesos, criterios,criterios custo e criterios beneficio com base no numero de criterios
            double[] pesos = new double[vecCritAlt[0]];
            String[] crt_beneficio = new String[vecCritAlt[0]];
            String[] crt_custo = new String[vecCritAlt[0]];
            String[] criterios = new String[vecCritAlt[0]];
            //Cria um vetor de strings para registar as alternativas com base numero das mesmas
            String[] alternativas = new String[vecCritAlt[1]];
            //Preenche as matrizes criadas anteriormente com base no ficheiro de texto
            lerFicheiroTopsis(crt_beneficio, crt_custo, alternativas, criterios, pesos, matrizTOPSIS, nomeFicheiro, logErros);
            String[] valorizacao = new String[vecCritAlt[0]];
            //Cria um vetor cujo objetivo é preencher com "+" caso o critério seja beneficio e "-" caso o critério seja custo (caso nao estejam especificados o vetor é todo preeenchido com "+"
            converterMaisMenos(crt_custo, valorizacao);
            //Resolve o problema por utilizando o método TOPSIS e lista o necessário tanto para a consola como para o ficheiro.
            resolverTOPSIS(matrizTOPSIS, pesos, valorizacao, alternativas, criterios, nomeFicheiro, nomeFicheiroOutput);
            System.out.println("ISEP Instituto Superior de Engenharia do Porto " + data + ".");
        }
        LogErros.fecharFileLogErros(logErros);
        writeFile.close();
    }


    public static int[] calculoCritAlt(String opCLI, String nomeficheiro) throws FileNotFoundException {
        int[] vecCritAlt = new int[2]; //vec[0] é o nº de criterios e vec[1] é o nº de alternativas
        Scanner ler = new Scanner(new File(nomeficheiro));
        int i = 0;
        while (ler.hasNextLine() && (i < 2)) {
            String linha = ler.nextLine();
            if (linha.length() > 0) { //linhas em branco
                String linhaSemEspacosRepetidos = linha.trim().replaceAll("( )+", " ");
                String[] contLinha = linhaSemEspacosRepetidos.split(" ");
                if (opCLI.equals("2")) { //TOPSIS
                    if (contLinha[0].equals("crt")) {
                        vecCritAlt[i] = contLinha.length - 1;
                        i++;
                    } else if (contLinha[0].equals("alt")) {
                        vecCritAlt[i] = contLinha.length - 1;
                        i++;
                    }
                } else { //AHP  
                    if ((contLinha[0].charAt(0) == 'm') && i < 2) {
                        vecCritAlt[i] = contLinha.length - 1;
                        i++;
                    }

                }
            }
        }
        return vecCritAlt;
    }

    /**
     * Metodo utilizado para fazer veriicacoes sobre as opcoes passadas na linha de comandos
     *
     * @param args
     * @param writeFile
     * @param nomeFicheiro
     * @throws FileNotFoundException
     * @return Se for retornado 1 faz-se o AHP, 2 o TOPSIS
     */
    public static String controlaOpcoesCLI(String[] args, Formatter writeFile, String nomeFicheiro) throws FileNotFoundException {
        String op = null;
        boolean hasM = false;
        String argDeM = null;

        if (args[0].charAt(0) == '-') {
            //Inicia com uma opção (-M é necessário)
            for (int i = 0; i < args.length; i++) {
                if (args[i].equals("-M")) {
                    if (!(args[i + 1].equals("1") || args[i + 1].equals("2"))) {
                        System.out.println("A opção -M requer os argumentos: 1 OU 2.");
                        System.exit(2);
                    } else {
                        hasM = true;
                        argDeM = args[i + 1];
                        break;
                    }
                }
            }

            if (hasM) {
                //Contem a opção obrigatória -M com um argumento válido

                //Se um limiar de consistencia foi definido, guardamo-lo
                for (int i = 0; i < args.length; i++) {
                    if (args[i].equals("-S")) {
                        limiarConsistencia = Double.parseDouble(args[i + 1]);
                    }
                }

                //Se um limiar de importancia foi definido para o metodo AHP, guardamo-lo
                for (int i = 0; i < args.length; i++) {
                    if (args[i].equals("-L")) {

                        if (argDeM.equals("1")) {
                            limiarImportancia = Double.parseDouble(args[i + 1]);
                        } else {
                            System.out.println("A opção -L só pode ser especificada para o argumento 1 da opção -M");
                            System.exit(2);
                        }
                    }
                }
            }

        } else {
            System.out.println("Deve especificar a opção obrigatória -M {1,2} para correr o programa.");
            System.exit(2);
        }

        if (argDeM.equals("1")) {
            op = "1";

        } else if (argDeM.equals("2")) {
            op = "2";
        }

        return op;
    }

    /**
     * Este metodo determina qual a opcao e o valor maior num conjunto de valores num determinado vetor
     *
     * @param vec vetor qualquer
     * @return o maior valor no vetor e a opcao
     */
    public static double[] determinarOpIdeal(double[] vec) {
        double[] sol = new double[2];
        double maior = vec[0];
        int opcao = 1;
        for (int cont = 1; cont < vec.length; cont++) {
            if (vec[cont] > maior) {
                maior = vec[cont];
                opcao = cont + 1;
            }
        }
        sol[0] = opcao;
        sol[1] = maior;
        return sol;
    }

    /**
     * Menu que permite ao utilizador escolher a opcao desejada
     *
     * @return opcao
     */
    public static String menu() {
        System.out.println("Selecione uma opcao: ");
        System.out.println("1-Calcular pelo metodo de aproximacao"
                + "\n2-Calcular pelo metodo exato (uso da biblioteca la4j)");
        System.out.print("Opcao: ");
        String op = sc.nextLine();
        return verOp(op);
    }

    /**
     * Verifica a opcao
     *
     * @param op opcao
     * @return opcao
     */
    public static String verOp(String op) {
        while (!("1".equals(op) || "2".equals(op) || "0".equals(op))) {
            //Enquanto a opcao nao for um digito valido
            if (!Character.isDigit(op.charAt(0))) {
                //Se n for um digito
                System.err.println("'" + op + "'" + " nao e um numero inteiro decimal");
            } else if (op.length() > 1) {
                //Se tiver mais que um digito
                System.err.println("Deve introduzir um digito apenas");
            } else {
                System.err.println("'" + op + "'" + " nao e um inteiro valido");
            }
            System.out.println("Tente de novo");
            System.out.print("Opcao: ");
            op = sc.nextLine();
        }
        return op;
    }
}
