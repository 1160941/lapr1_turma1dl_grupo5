/**
 * Classe de metodos
 * com utilidade geral
 */
package lapr1;

import java.util.Calendar;

public class Util {

    /**
     * Metodo que multiplica uma matriz e um vetor
     *
     * @param mat Matriz a multiplicar
     * @param vec Vetor a multiplicar
     * @return produto
     */
    public static double[] multiplicarArrays(double[][] mat, double[] vec) {
        double sum = 0;
        double[] res = new double[mat.length];

        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[0].length; j++) {
                sum += vec[j] * mat[i][j];
            }
            res[i] = sum;
            sum = 0;
        }
        return res;
    }

    /**
     * Multiplica uma matriz por um vetor
     *
     * @param mat matriz que contem os vetores de prioridade composta
     * @param vecPesos vetor dos Pesos / vetor próprio da matriz dos critérios
     * @return resultado da operação
     */
    public static double[] multVecPrioridadeComposta(double[][] mat, double[] vecPesos) {
        double[] opVec = new double[mat.length];
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[0].length; j++) {
                opVec[i] += vecPesos[j] * mat[i][j];
            }
        }

        return opVec;
    }

    /**
     * Eleva todos os elementos de uma matriz ao quadrado
     *
     * @param mat
     */
    public static void elevarElementosMatAoQuadrado(double[][] mat, double[][] matAoQuadrado) {
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[0].length; j++) {
                matAoQuadrado[i][j] = Math.pow(mat[i][j], 2);
            }
        }
    }

    /**
     * Calcula o IR
     *
     * @param mat matriz
     * @return ir relativo a matriz
     */
    public static double calcIR(double[][] mat) {
        int[] ValRefIndice = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        double[] ValRefCons = {0.00, 0.00, 0.58, 0.90, 1.12, 1.24, 1.32, 1.41, 1.45, 1.49};
        double IR = 0;
        int n = (mat.length);
        for (int i = 0; i < ValRefIndice.length; i++) {
            if (n == ValRefIndice[i]) {
                IR = ValRefCons[i];
            }
        }
        return IR;
    }

    /**
     * Metodo que cria um vetor com a soma das colunas
     *
     * @param mat - Matriz original da comparacao dos criterios
     * @param ordem ordem da matriz
     * @return vetor resultante da soma da coluna da matriz
     */
    public static double[] criarVetorSomaCol(double[][] mat, int ordem) {
        //Var,obj
        double somaCol = 0;
        double[] vecSoma = new double[ordem];
        for (int j = 0; j < ordem; j++) {
            for (int i = 0; i < ordem; i++) {
                somaCol += mat[i][j];
            }
            vecSoma[j] = somaCol;
            somaCol = 0;
        }
        return vecSoma;
    }

    /**
     * Encontra o maior valor de uma matriz e retorna-o
     *
     * @param mat
     * @return o maximo de uma matriz
     */
    public static double findMax(double[][] mat) {
        double max = 0;
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[0].length; j++) {
                if (mat[i][j] > max) {
                    max = mat[i][j];
                }
            }
        }
        return max;
    }

    /**
     * Metodo que encontra o maior valor de um vetor, juntamente com o seu indice
     *
     * @param vec
     * @return
     */
    public static double[] findMaxAndIndex(double[] vec) {
        double max = 0;
        double index = -1;
        for (int i = 0; i < vec.length; i++) {
            if (vec[i] > max) {
                max = vec[i];
                index = i;
            }
        }
        double[] sol = {index, max};
        return sol;
    }

    /**
     * Divide dois valores
     *
     * @param numerador
     * @param denominador
     * @return divisao
     */
    public static double dividir(double numerador, double denominador) {
        double res = 0;
        if (denominador != 0) {
            res = numerador / denominador;
        } else {
            System.err.println("O denominador nao pode ser nulo.");
        }
        return res;
    }

    /**
     * Metodo que soma as linhas e as coloca num vetor Neste caso, permite obter o resultado final da multiplicacao de 2 matrizes, pois a multiplicacao intermedia ja tinha sido feita
     *
     * @param matAux - vetor da multiplicacao de colunas da matriz dos criterios pela respetiva linha do vetor proprio
     * @param ordem
     * @return
     */
    public static double[] criarVetorSomaLin(double[][] matAux, int ordem) {
        //Var,obj
        double somaLin = 0;
        double[] multiResult = new double[ordem];
        for (int i = 0; i < ordem; i++) {
            for (int j = 0; j < ordem; j++) {
                somaLin += matAux[i][j];
            }
            multiResult[i] = somaLin;
            somaLin = 0;
        }
        return multiResult;
    }

    public static void inverterString(String[] nome) {
        for (int i = 0; i < nome.length; i++) {
            if (nome[i].equals("+")) {
                nome[i] = "-";
            } else {
                nome[i] = "+";
            }
        }
    }

    public static boolean isDigit(String[] contLinha) {
        boolean isDigit = true;
        for (int i = 0; i < contLinha.length; i++) {
            if (!Character.isDigit(contLinha[i].charAt(0))) {
                isDigit = false;
                break;
            }
        }
        return isDigit;
    }

    /**
     * Remove matriz relativamente ao critério
     *
     * @param matrix3D
     * @param pos
     * @param nCrit
     * @return
     */
    public static double[][][] removeMatrix(double[][][] matrix3D, int pos, int nCrit, int nAlt) {
        double[][][] newMatrix3D = new double[nCrit + 1][][];
        newMatrix3D[0] = matrix3D[0];
        for (int i = 1; i < nCrit + 1; i++) {
            newMatrix3D[i] = new double[nAlt][nAlt];
        }
        if (pos == nCrit) {
            for (int i = 1; i < nCrit + 1; i++) {
                newMatrix3D[i] = matrix3D[i];
            }
        } else {
            for (int k = 0; k < nCrit + 1; k++) {
                newMatrix3D[k] = matrix3D[k];

            }
            for (int i = pos; i < nCrit + 1; i++) {
                newMatrix3D[i] = matrix3D[i + 1];
            }
        }
        return newMatrix3D;
    }

    /**
     * Remove a linha e coluna relativa a um critério
     *
     * @param matrix
     * @param pos indice do critério
     * @return a nova matriz sem o critério
     */
    public static double[][] removLinhaColdeMatrix(double[][] matrix, int pos) {
        double[][] newMat = new double[matrix.length - 1][matrix[0].length - 1];
        for (int i = 0; i < (matrix.length - 1); i++) {
            if (i >= pos) {
                for (int j = 0; j < (matrix[pos].length - 1); j++) {
                    if (j >= pos) {
                        newMat[i][j] = matrix[i + 1][j + 1];
                    } else {
                        newMat[i][j] = matrix[i + 1][j];
                    }
                }
            } else {
                for (int j = 0; j < (matrix[pos].length - 1); j++) {
                    if (j >= pos) {
                        newMat[i][j] = matrix[i][j + 1];
                    } else {
                        newMat[i][j] = matrix[i][j];
                    }
                }
            }
        }
        return newMat;
    }

    /**
     * Transpõe uma matriz
     *
     * @param m matriz
     * @return matriz composta
     */
    public static double[][] transposeMatrix(double[][] m) {
        double[][] temp = new double[m[0].length][m.length];
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[0].length; j++) {
                temp[j][i] = m[i][j];
            }
        }
        return temp;
    }

    /**
     * Metodo que normaliza qualquer matriz, dividindo os elementos de cada coluna pela soma respetiva
     *
     * @param vecSoma - Vetor que guarda a soma das colunas, calculadas previamente
     * @param mat matriz
     * @param ordem ordem da matriz
     * @return
     */
    public static double[][] normalizarMatriz(double[] vecSoma, double[][] mat, int ordem) {
        //Var,obj
        double[][] matN = new double[ordem][ordem];

        for (int j = 0; j < ordem; j++) {
            for (int i = 0; i < ordem; i++) {
                matN[i][j] = mat[i][j] / vecSoma[j];
            }
        }
        return matN;
    }

    public static double[] getVectorSymmetry(double[] vec) {
        for (int i = 0; i < vec.length; i++) {
            if (vec[i] > 0) {
                return vec;
            }
        }
        for (int i = 0; i < vec.length; i++) {
            vec[i] *= - 1;
        }

        return vec;
    }

    /**
     * Data do dia e as horas
     *
     * @return
     */
    public static String getDayMonthYearHour() {
        String[] month = {"janeiro", "fevereiro", "março", "abril", "maio", "junho", "julho", "agosto", "setembro", "outubro", "novembro", "dezembro"};
        Calendar hoje = Calendar.getInstance();
        int diaH = hoje.get(Calendar.DAY_OF_MONTH);
        int mesH = hoje.get(Calendar.MONTH);
        int anoH = hoje.get(Calendar.YEAR);
        int hora = hoje.get(Calendar.HOUR);
        int minuto = hoje.get(Calendar.MINUTE);
        int segundo = hoje.get(Calendar.SECOND);
        return (diaH + " de " + month[mesH] + " de " + anoH + " Hora " + hora + "h " + minuto + "m " + segundo + "s");
    }

}
