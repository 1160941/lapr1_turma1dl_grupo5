/**
 * Classe de metodos associados
 * a mostra de informação
 */
package lapr1;

import java.util.Formatter;
import static lapr1.AHP.calcRC;
import static lapr1.Util.calcIR;
import static lapr1.AHP.*;
import static lapr1.Util.findMaxAndIndex;

public class List {

    /**
     *
     * @param txt
     * @param mat3d
     * @param vecCritAlt
     * @param form
     * @param nomeCrits
     */
    public static void listarMats(String txt, double[][][] mat3d, int[] vecCritAlt, Formatter form, String[] nomeCrits) {

        int indCabecalho = 0;//
        int indice = 0;//
        form.format("%n%n", "");
        form.format("%-6s", txt);
        form.format("%n%n", "");
        form.format("%-20s", "mc_critérios");

        for (int linhaVec = 0; linhaVec <= vecCritAlt[0]; linhaVec++) {
            form.format("%n", "");
            if ((indCabecalho > 1) && (indice < nomeCrits.length)) {
                form.format("%-20s", "mcp_" + nomeCrits[indice]);
                indice++;
            }
            indCabecalho++;
            int linhaMat = 0;
            while (linhaMat < mat3d[linhaVec].length) {
                int col = 0;
                form.format("%n%-14s", "");
                while (col < mat3d[linhaVec][linhaMat].length) {
                    if (!(mat3d[linhaVec][linhaMat][col] == 0.0)) {
                        form.format("%-23s", mat3d[linhaVec][linhaMat][col]);
                    }
                    col++;
                }
                form.format("%-15s", "");
                form.format("%n%-15s", "");
                linhaMat++;
            }
            indCabecalho++;
            form.format("%n", "");
            form.format("%n", "");
        }

    }

    public static void listarVecComCabecalho(String nomeVec, Formatter form, double[] vec, String[] Cabecalho) {
        form.format("%n", "");
        form.format("%5s%n%n", nomeVec);
        for (int i = 0; i < vec.length; i++) {
            form.format("%-15s", Cabecalho[i]);
            form.format("%-15s%n", vec[i]);
        }
        form.format("%n", "");
    }

    /**
     *
     * @param file
     * @param rcVec
     * @param irVec
     * @param lambdaVec
     * @param nomeCrits
     * @param vecCritAlt
     */
    public static void listarLambdaMaxIRRCExato(Formatter file, double[] rcVec, double[] irVec, double[] lambdaVec, String[] nomeCrits, int vecCritAlt[]) {
        file.format("%-20s", "mc_critérios");
        for (int i = 0; i <= vecCritAlt[0]; i++) {

            if (i >= 1) {
                file.format("\nmcp_" + nomeCrits[i - 1]);
            }
            file.format("\nRC:" + rcVec[i]);
            file.format("\nIR:" + irVec[i]);
            file.format("\nValor proprio maximo: " + lambdaVec[i]);

            file.format("%n", "");
        }
    }

    /**
     * Lista uma matriz de double com cabeçalho e lombar.
     *
     * @param txt
     * @param mat
     * @param form
     */
    public static void listarMatsDoubleComCriteAlt(String txt, double[][] mat, String[] Criterios, String[] Alternativas, Formatter form) {
        form.format("%6s", txt);
        form.format("%n%n");
        for (int i = 0; i < mat[0].length; i++) {
            form.format("%-25s", Criterios[i]);
        }
        form.format("%n");
        for (int i = 0; i < mat.length; i++) {
            form.format("%n%n", "");
            for (int j = 0; j < mat[0].length; j++) {
                form.format("%-25s", mat[i][j]);
            }
            form.format("%-25s", Alternativas[i]);
        }
        form.format("%n%n%n");
    }

    /**
     * Lista um vetor qualquer
     *
     * @param nomeVec nome do vetor
     * @param form formatter
     * @param vec vetor
     */
    public static void listarVec(String nomeVec, Formatter form, double[] vec) {
        form.format("%n", "");
        form.format("%5s%n", nomeVec);
        for (int i = 0; i < vec.length; i++) {
            form.format("%5s%n", vec[i]);
        }
        form.format("%n", "");
    }

    /**
     * Lista o RC,IC e o lambda maximo de cada matriz (aproximado)
     *
     * @param file
     * @param mat3D
     * @param nomeCrits
     */
    public static void listarLambdaIRRCAproximados(Formatter file, double[][][] mat3D, String[] nomeCrits) {
        //Var,obj
        double maxEigenValue;
        file.format("%-20s", "mc_critérios");

        for (int i = 0; i < mat3D.length; i++) {

            if (i >= 1) {
                file.format("\n" + "mcp_" + nomeCrits[i - 1]);
            }

            maxEigenValue = calcValorProprioMaximoAproximado(mat3D[i]);

            file.format("\nRC:" + calcRC(mat3D[i], maxEigenValue));
            file.format("\nIR:" + calcIR(mat3D[i]));
            file.format("\nValor proprio maximo: " + maxEigenValue);
            file.format("%n", "");
        }

    }

    /**
     * Lista as matrizes normalizadas seguidas do vetor de prioridade composta
     *
     * @param file
     * @param txt texto que descreve o que se vai fazer
     * @param matNorm conjunto de matrizes normalizadas dentro de um vetor
     * @param matPriorComp conjunto de vetores de prioridade composta das matrizes
     */
    public static void listarMatNormVetComp(Formatter file, String txt, double[][][] matNorm, double[] vecPesosComp, double[][] matPriorComp, String[] nomeCrits) {
        file.format("%50s", txt);
        System.out.println();
        int indCabecalho = 0;
        file.format("%-20s", "mc_critérios");
        file.format("%n%n", "");
        for (int i = 0; i < matNorm.length; i++) {//Seleciona a matriz a usar da mat3D
            if (indCabecalho >= 1) {
                file.format("\nmcp_" + nomeCrits[i - 1]);
                file.format("%n%n", "");
            }

            for (int j = 0; j < matNorm[i].length; j++) {
                for (int k = 0; k < matNorm[i][j].length; k++) {
                    file.format("%-25s", matNorm[i][j][k] + " ");
                }
                if (i == 0) {
                    file.format(" |" + vecPesosComp[j]);
                    file.format("%n", "");
                } else {
                    file.format(" |" + matPriorComp[j][i - 1]);
                    file.format("%n", "");
                }
            }
            file.format("%n%n", "");

            indCabecalho++;
        }
    }

    /**
     * Escreve a opcao adequada
     *
     * @param txt
     * @param sol vetor que contem a solucao e a opcao
     * @param out formatter
     */
    public static void printOpcaoAdequada(String txt, double[] vecPriorComposta, Formatter out) {
        double[] sol = findMaxAndIndex(vecPriorComposta);
        out.format("%n%5s", txt + (int) (sol[0] + 1) + " com " + Math.round(sol[1] * 100) + "% de importancia relativa.");
        out.format("%n");
    }

}
