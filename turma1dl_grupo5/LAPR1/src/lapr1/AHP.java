package lapr1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;

import static lapr1.Main.*;
import static lapr1.Util.*;

import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;

/**
 * Classe que contem a implementação do método AHP
 */
public class AHP {

    private final static String FICH_LOG_ERROS_AHP = getDayMonthYearHour() + "AHP.txt";

    /**
     * Faz a leitura de um ficheiro relativo ao método AHP
     *
     *
     * @param nomeCrits Vetor que guarda o nome dos critérios
     * @param nomeAlts Vetor que guarda o nome das alternativas
     * @param vecCritAlt Vetor que guarda o número de critérios e o número de alternativas
     * @return vetor de matrizes que contém os dados de todas as matrizes
     * @throws FileNotFoundException
     */
    public static double[][][] lerFicheiroMatrizesAHP(String[] nomeCrits, String[] nomeAlts, String nomeficheiro, int[] vecCritAlt,Formatter logErros) throws FileNotFoundException {
        int linhaVec3D = 0;
        int linhaMat = 0;
        int flagCabeNovo = 0;
        double[][][] mat3D = new double[vecCritAlt[0] + 1][][];

        mat3D[0] = new double[vecCritAlt[0]][vecCritAlt[0]];
        for (int i = 1; i < mat3D.length; i++) {
            mat3D[i] = new double[vecCritAlt[1]][vecCritAlt[1]];
        }

        Scanner ler = new Scanner(new File(nomeficheiro));

        while (ler.hasNextLine()) {
            String linha = ler.nextLine();
            if (linha.length() > 0) { //linhas em branco
                String linhaSemEspacosRepetidos = linha.trim().replaceAll("( )+", " ");
                String[] contLinha = linhaSemEspacosRepetidos.split(" ");
                //No metodo ahp as linhas que nao sao nem para o preenchimento das matrizes nem de cabecalho(linhas nao completamente em branco)
                if (!(isDigit(contLinha))) { //Ver que é string
                    if (contLinha[0].equalsIgnoreCase("mc_criterios") && (contLinha.length - 1 == vecCritAlt[0])) { //assegurar que as linhas nao estao completamente em branco na matriz dos pesos

                        for (int i = 0; i < nomeCrits.length; i++) {
                            nomeCrits[i] = contLinha[i + 1];
                        }

                    } else if ((validNomeAlt(contLinha[0], nomeCrits) == true) || (contLinha.length - 1 == vecCritAlt[1])) { //ver se a linha é uma cabecalho da matriz de comparacao das alternativas
                        for (int i = 0; i < nomeAlts.length; i++) {
                            nomeAlts[i] = contLinha[i + 1];
                        }
                        flagCabeNovo = 1;
                        linhaMat = 0;
                    } else {
                        LogErros.registarErro(logErros, linha, "Método AHP: Linha inválida");
                        logErros.close();
                        System.exit(1);
                    }
                } else if ((vecCritAlt[1] == contLinha.length) || (contLinha.length == vecCritAlt[0])) {
                    linhaVec3D = guardarDadosAHP(linhaMat, flagCabeNovo, mat3D, contLinha, linhaVec3D);
                    linhaMat++;
                    flagCabeNovo = 0; //Se no caso de ter passado a escrever para uma matriz nova então é preciso renovar a flag

                } else {
                    LogErros.registarErro(logErros, linha, "Método AHP: Linha inválida");
                    logErros.close();
                    System.exit(1);
                }

            }
        }
        ler.close();
        return mat3D;
    }

    /**
     * Este metodo passa uma linha retirada do ficheiro para o vetor de matrizes
     *
     * @param flag - flag que assinala a transferência dos dados para uma nova matriz
     * @param mat3D - vetor de matrizes que guarda os dados numéricos do ficheiro
     * @param contLinha - vetor que contém o conteudo de cada linha
     * @param linhaVecFich - A linha que diz a matriz que se está a preencher
     * @return a linha do vetor de 3 dimensoes
     */
    public static int guardarDadosAHP(int linhaMat, int flag, double[][][] mat3D, String[] contLinha, int linhaVecFich) {

        String[] vecDiv;
        double[] vecTemp = new double[contLinha.length];
        //percorrer o vetor vecTemp e ver se existe algum elemento que tenha um número a dividir por outro (que não é válido em double)

        for (int i = 0; i < contLinha.length; i++) {
            vecDiv = contLinha[i].split("/");
            if (vecDiv.length == 2) {
                vecTemp[i] = (double) ((Double.parseDouble(vecDiv[0])) / (Double.parseDouble(vecDiv[1])));
            } else {
                vecTemp[i] = Double.parseDouble(contLinha[i]);
            }
        }
        if (flag == 1) { //Quando encontrar uma linha nova de cabecalho ativa a flag para passar a escrever para uma nova matriz
            linhaVecFich++;

        }

        mat3D[linhaVecFich][linhaMat] = vecTemp; //quando encontrar insere a linha retirada do ficheiro

        return linhaVecFich;
    }

    /**
     * Indica se a linha corresponde ao nome de um dos cabecalhos de uma das matrizes de comparação de alternativas
     *
     * @param cabecalhoMatAlt elemento do vetor da linha que corresponde ao nome da matriz de comparação de alternativas
     * @param nomeCrits vetor que guarda o nome de cada critério
     * @return verdadeiro ou falso
     */
    public static boolean validNomeAlt(String cabecalhoMatAlt, String[] nomeCrits) {
        boolean flag = false;
        String[] cabecalhoUnder = cabecalhoMatAlt.split("_");
        if (cabecalhoUnder.length > 1) {
            for (int i = 0; i < nomeCrits.length; i++) {
                if (cabecalhoUnder[1].equals(nomeCrits[i])) {
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }

    /**
     * Normaliza todas as matrizes
     *
     * @param mat3d - vetor de matrizes que guarda os dados numéricos do ficheiro
     * @param vecCritAlt -Vetor que guarda o número de critérios e o número de alternativas
     * @return
     */
    public static double[][][] normMat3d(double[][][] mat3d, int[] vecCritAlt) {
        double[][][] mat3dNorm = new double[vecCritAlt[0] + 1][Math.max(vecCritAlt[0], vecCritAlt[1])][Math.max(vecCritAlt[0], vecCritAlt[1])];
        for (int i = 0; i < vecCritAlt[0] + 1; i++) {
            if (i == 0) {
                mat3dNorm[i] = construirMatrizNormalizadaAHP(mat3d[i], Util.criarVetorSomaCol(mat3d[i], vecCritAlt[0]));
            } else {
                mat3dNorm[i] = construirMatrizNormalizadaAHP(mat3d[i], Util.criarVetorSomaCol(mat3d[i], vecCritAlt[1]));
            }

        }
        return mat3dNorm;
    }

    /**
     * Preenche uma matriz normalizada com a matriz de entrada (método TOPSIS)
     *
     * @param mat
     * @param soma
     * @return
     */
    public static double[][] construirMatrizNormalizadaAHP(double[][] mat, double[] soma) {
        double[][] matNorm = new double[mat.length][mat[0].length];
        for (int j = 0; j < mat[0].length; j++) {
            for (int i = 0; i < mat.length; i++) {
                if (mat[i][j] != 0.0) {
                    matNorm[i][j] = (double) (mat[i][j] / soma[j]);
                }

            }
        }
        return matNorm;
    }

    /**
     * Verifica se a matriz é consistente no método AHP
     *
     *
     * @param mat3D Vetor de matrizes
     * @param rcVec vetor que guarda os valores de RC
     * @param lambdaVec vetor que guarda o valor de lambda máximo
     * @return
     * @throws java.io.FileNotFoundException
     */
    public static boolean ifMatCons(double[][][] mat3D, double[] rcVec, double[] lambdaVec) throws FileNotFoundException {
        Formatter logErros = LogErros.criarEAbrirFileLogErros(FICH_LOG_ERROS_AHP);
        double RC, lambdaMax;
        boolean eConsistente = true;
        for (int i = 0; i < mat3D.length; i++) {
            lambdaMax = calcMaiorValorProprioExato(mat3D[i]);
            if (mat3D[i].length > 2 && mat3D[i].length < 10) { //A ordem da matriz está bem
                RC = calcRC(mat3D[i], lambdaMax);
                if (RC < limiarConsistencia) {
                    rcVec[i] = RC;
                    lambdaVec[i] = lambdaMax;
                } else {
                    eConsistente = false;
                    LogErros.registarErro(logErros, Util.getDayMonthYearHour(), "A matriz não é consistente");
                    break;
                }
            } else {
                LogErros.registarErro(logErros, Util.getDayMonthYearHour(), "Impossível calcular o IR devido à ordem da matriz estar compreendida entre valores inferiores a 3 ou superiores a 16.");
            }

        }
        LogErros.fecharFileLogErros(logErros);
        return eConsistente;
    }

    /**
     * Calcula o RC
     *
     * @param mat matriz sobre a qual incide a avaliacao de consistencia
     * @param maiorValorProprio
     * @return
     */
    public static double calcRC(double[][] mat, double maiorValorProprio) {
        double IC = dividir(maiorValorProprio - mat.length, mat.length - 1);
        double IR = calcIR(mat);
        double RC = dividir(IC, IR);

        return RC;
    }

    /**
     * Metodo que [por via de decomposicao de uma matriz em valores e vetores proprios] encontra e retorna o
     *
     * @param matriz
     * @return maior valor proprio
     */
    public static double calcMaiorValorProprioExato(double[][] matriz) {
        Matrix mat = new Basic2DMatrix(matriz);

        //Decompomos a matriz em valores e vetores proprios
        EigenDecompositor eigenD = new EigenDecompositor(mat);
        Matrix[] mattD = eigenD.decompose();

        double eigenValues[][] = mattD[1].toDenseMatrix().toArray();
        //Matriz que contem os valores proprios

        double maxEigenValue = findMax(eigenValues);
        return maxEigenValue;
    }

    /**
     * Calcula o vetor proprio a partir de la4j
     *
     * @param matriz uma matriz qualquer
     * @return vetor das prioridades
     */
    public static double[] calcExactEigenVector(double[][] matriz) {
        int indexMaior;
        Matrix x = new Basic2DMatrix(matriz);
        EigenDecompositor eigenD = new EigenDecompositor(x);
        Matrix[] dec = eigenD.decompose();
        double A[][] = dec[0].toDenseMatrix().toArray(); //matriz que contem os vetores proprios
        double D[][] = dec[1].toDenseMatrix().toArray(); //matriz que contem os valores proprios
        indexMaior = determinarMaiorElemDiagonal(D);
        double[] vecDasPrioridades = new double[D[0].length];
        for (int i = 0; i < D[0].length; i++) {
            vecDasPrioridades[i] = A[i][indexMaior];
        }
        vecDasPrioridades = getVectorSymmetry(vecDasPrioridades);

        return vecDasPrioridades;
    }

    /**
     * Determina o maior valor proprio
     *
     * @param matrix
     * @return o indice da coluna onde esta o maior valor proprio
     */
    public static int determinarMaiorElemDiagonal(double[][] matrix) {
        double maior = matrix[0][0];
        int index = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (i == j) {
                    if (matrix[i][j] < 0) {
                        matrix[i][j] = matrix[i][j] * (-1);
                    }
                    if (matrix[i][j] > maior) {
                        maior = matrix[i][j];
                        index = j;
                    }
                }
            }
        }
        return index;
    }

    /**
     * método que preenche um vetor com todos os valores de IR de cada matriz
     *
     * @param irVec Vetor que guarda os valores do IR de cada matriz
     * @param mat3d matriz que contém todos os dados relativamente a todas as matrizes
     */
    public static void preenchVetIR(double[] irVec, double[][][] mat3d) {
        for (int i = 0; i < mat3d.length; i++) {
            irVec[i] = calcIR(mat3d[i]);
        }
    }

    public static int procuraLimiarPesos(double[] vecPesos, double limiarImportancia) {
        int pos = -1;
        for (int i = 0; i < vecPesos.length; i++) {
            if (vecPesos[i] < limiarImportancia) {
                pos = i;
            }
        }
        return pos;
    }

    public static double[] criarVecRC(double[][][] mat3D) throws FileNotFoundException {
        double[] vecRC = new double[mat3D.length];
        for (int i = 0; i < mat3D.length; i++) {
            if (mat3D[i].length <= 2 || mat3D[i].length >= 10) {
                Formatter logErros = LogErros.criarEAbrirFileLogErros(FICH_LOG_ERROS_AHP);
                LogErros.registarErro(logErros, "IR: " + vecRC[i], "Ordem da matriz não suportada para o calculo do IR.\nO programa irá terminar.");
                logErros.close();
                System.exit(1);
            } else {
                vecRC[i] = calcRC(mat3D[i], calcValorProprioMaximoAproximado(mat3D[i]));
            }
        }
        return vecRC;
    }

    /**
     * Metodo para determinar uma aproximacao ao valor proprio maximo
     *
     * @param mat matriz
     * @return valor proprio maximo
     */
    public static double calcValorProprioMaximoAproximado(double[][] mat) {
        double[] eigenVec;
        double[] AX; //in A * X = lambdaMax * X
        double maxEigenValue;
        double sum = 0;
        eigenVec = criarVetorProprioAproximado(mat);
        AX = multiplicarArrays(mat, eigenVec);

        for (int i = 0; i < mat.length; i++) {
            sum += AX[i] / eigenVec[i];
        }

        maxEigenValue = (double) sum / mat.length;

        return maxEigenValue;
    }

    /**
     * Metodo que cria um vetor proprio com valores aproximados
     *
     * @param mat matriz de comparacao de criterios
     * @return vetor proprio
     */
    public static double[] criarVetorProprioAproximado(double[][] mat) {
        //Arrays
        double[] vecSomaCol = criarVetorSomaCol(mat, mat.length);
        double[][] matNormal = normalizarMatriz(vecSomaCol, mat, mat.length);
        double[] vetorProprio = new double[mat.length];

        vetorProprio = criarVetorSomaLin(matNormal, mat.length);

        for (int i = 0; i < mat.length; i++) {
            vetorProprio[i] /= mat.length;
        }

        return vetorProprio;
    }

}
