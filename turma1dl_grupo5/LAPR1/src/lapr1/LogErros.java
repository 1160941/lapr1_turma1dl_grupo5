package lapr1;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;

public class LogErros {
/*
    @ param nomeFichLogErros - Nome do ficheiro de log de erros*/
    /*Criar um ficheiro associado ao log de erros*/
    static Formatter criarEAbrirFileLogErros(String nomeFichLogErros) throws FileNotFoundException {
        Formatter output = new Formatter(new File(nomeFichLogErros));
        return output;
    }
    /*Fechar um ficheiro associado ao log de erros*/
    static void fecharFileLogErros(Formatter nomeFich) {
     nomeFich.close();
    }
    /*Registar erro num ficheiro associado ao log de erros*/
    static void registarErro(Formatter out , String linha, String txt) {
        out.format("ocorreu erro na linha : %s - %s %n",linha,txt);
    }
}