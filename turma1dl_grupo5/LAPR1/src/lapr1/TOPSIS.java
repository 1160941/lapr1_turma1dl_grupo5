package lapr1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;
import static lapr1.LogErros.registarErro;

/**
 * Classe que contem a implementação do método TOPSIS
 */
public class TOPSIS {

    public static void resolverTOPSIS(double[][] matriz, double[] pesos, String[] valorizacao, String[] alternativas, String[] criterios, String nomeficheiro, String output) throws FileNotFoundException {
        Formatter topsisFile = new Formatter(new File(output));
        Formatter topsisConsole = new Formatter(System.out);
        topsisFile.format("Este problema foi resolvido utilizando o método TOPSIS.");
        topsisConsole.format("Este problema foi resolvido utilizando o método TOPSIS.");
        //Lista os dados de entrada (Vetor pesos e a matriz) para a consola e o ficheiro
        List.listarVecComCabecalho("Vetor pesos:", topsisFile, pesos, criterios);
        List.listarVecComCabecalho("Vetor pesos:", topsisConsole, pesos, criterios);
        List.listarMatsDoubleComCriteAlt("Matriz de decisão:", matriz, criterios, alternativas, topsisConsole);
        List.listarMatsDoubleComCriteAlt("Matriz de decisão:", matriz, criterios, alternativas, topsisFile);
        //Normaliza a matriz
        normalizarMatriz(matriz);
        //Lista a matriz normalizada no ficheiro
        List.listarMatsDoubleComCriteAlt("Matriz normalizada:", matriz, criterios, alternativas, topsisFile);
        //Normaliza a matriz com os pesos
        contruirMatrizNormailizadaPesada(matriz, pesos);
        //Lista para a consola e o ficheiro a matriz normalizada pesada
        List.listarMatsDoubleComCriteAlt("Matriz normalizada pesada:", matriz, criterios, alternativas, topsisConsole);
        List.listarMatsDoubleComCriteAlt("Matriz normalizada pesada:", matriz, criterios, alternativas, topsisFile);
        double[] solucoesIdeais = new double[matriz[0].length];
        //Cria um vetor com as solucoes ideais e lista para o ficheiro
        vetorSolucoesIdeais(matriz, solucoesIdeais, valorizacao);
        List.listarVecComCabecalho("Soluções ideais:", topsisFile, solucoesIdeais, alternativas);
        double[] solucoesIdeaisNegativas = new double[matriz[0].length];
        //Inverte a valorização das Strings (troca + por -) para calcular as soluções ideais negativas
        Util.inverterString(valorizacao);
        //Cria um vetor com as solucoes ideais negativas e lista para o ficheiro
        vetorSolucoesIdeais(matriz, solucoesIdeaisNegativas, valorizacao);
        List.listarVecComCabecalho("Soluções ideais negativas:", topsisFile, solucoesIdeaisNegativas, alternativas);
        double[] ditSolucaoIdeal = new double[matriz.length];
        //Preenche um vetor com as distancias relativas à solução ideal e lista para o ficheiro
        vetorSeparacaodaSolucaoIdeal(matriz, solucoesIdeais, ditSolucaoIdeal);
        List.listarVec("Vetor com a distância entre cada alternativa e as soluções ideais:", topsisFile, ditSolucaoIdeal);
        //Preenche um vetor com as distancias relativas à solução ideal negativa e lista para o ficheiro
        double[] ditSolucaoIdealNegativa = new double[matriz.length];
        vetorSeparacaodaSolucaoIdeal(matriz, solucoesIdeaisNegativas, ditSolucaoIdealNegativa);
        List.listarVecComCabecalho("Vetor com a distância entre cada alternativa e as soluções ideais negativas:", topsisFile, ditSolucaoIdealNegativa, alternativas);
        double[] solucoesMaisPertodaIdeal = new double[matriz.length];
        //Preeenche um vetor com as próximidades relativas à solução ideal
        VetorSolucaoMaisPertodaIdeal(ditSolucaoIdeal, ditSolucaoIdealNegativa, solucoesMaisPertodaIdeal);
        //Imprime para a consola e para o ficheiro os vetores com as proximidades relativas à solução ideal e a respetiva solução ideal
        List.listarVecComCabecalho("Vetor com as próximidades relativas á solução ideal:", topsisConsole, solucoesMaisPertodaIdeal, alternativas);
        List.listarVecComCabecalho("Vetor com as próximidades relativas á solução ideal:", topsisFile, solucoesMaisPertodaIdeal, alternativas);
        printOpcaoAdequadaTOPSIS(solucoesMaisPertodaIdeal, alternativas, topsisConsole);
        printOpcaoAdequadaTOPSIS(solucoesMaisPertodaIdeal, alternativas, topsisFile);
        String data = Util.getDayMonthYearHour();
        topsisFile.format("ISEP Instituto Superior de Engenharia do Porto " + data + ".");
        topsisFile.close();
    }

    ;

    /**
     * Cria um vetor com a raiz quadrada da soma dos valores ao quadrado de cada
     * coluna
     *
     * @param mat
     */
    

    public static double[] criarVetorComSomadosElementosDeColunaaoQuadrado(double[][] matriz) {
        double[] somaRQdosQuadrados = new double[matriz[0].length];
        double[][] mataoQuadrado = new double[matriz.length][matriz[0].length];
        Util.elevarElementosMatAoQuadrado(matriz, mataoQuadrado);
        double soma = 0;
        for (int i = 0; i < (mataoQuadrado[0].length); i++) {
            for (int j = 0; j < mataoQuadrado.length; j++) {
                soma = soma + mataoQuadrado[j][i];
            }
            somaRQdosQuadrados[i] = Math.pow(soma, ((double) 1 / 2));
            soma = 0;
        }
        return somaRQdosQuadrados;
    }

    /**
     * Preenche uma matriz normalizada com a matriz de entrada (método TOPSIS)
     *
     * @param mat
     * @param mat
     * @param somaRQdosQuadrados
     */
    public static void normalizarMatriz(double[][] mat) {
        double[] somaRQdosQuadrados = criarVetorComSomadosElementosDeColunaaoQuadrado(mat);
        for (int j = 0; j < mat[0].length; j++) {
            for (int i = 0; i < mat.length; i++) {
                mat[i][j] = ((double) mat[i][j] / somaRQdosQuadrados[j]);
            }
        }
    }

    /**
     * Recebe a matriz normalizada e o vetor pesos e constroi a matriz. normalizada pesada
     *
     * @param matNormalizada
     * @param matNormalizada
     * @param pesos
     */
    public static void contruirMatrizNormailizadaPesada(double[][] matNormalizada, double[] pesos) {
        for (int j = 0; j < pesos.length; j++) {
            for (int i = 0; i < matNormalizada.length; i++) {
                matNormalizada[i][j] = matNormalizada[i][j] * pesos[j];
            }
        }
    }

    /**
     * Preenche um vetor com o maior ou menor valor de cada coluna.
     *
     * @param matNormalizadaPesada
     * @param solucoesIdeais
     * @param maioresoumenores
     */
    public static void vetorSolucoesIdeais(double[][] matNormalizadaPesada, double[] solucoesIdeais, String[] posiOUnega) {
        for (int h = 0; h < solucoesIdeais.length; h++) {
            solucoesIdeais[h] = matNormalizadaPesada[0][h];

            for (int i = 0; i < matNormalizadaPesada[0].length; i++) {
                if (posiOUnega[i].equals("+")) {
                    for (int j = 0; j < matNormalizadaPesada.length; j++) {
                        if (matNormalizadaPesada[j][i] > solucoesIdeais[i]) {
                            solucoesIdeais[i] = matNormalizadaPesada[j][i];
                        }
                    }
                } else {
                    for (int j = 0; j < matNormalizadaPesada.length; j++) {
                        if (matNormalizadaPesada[j][i] < solucoesIdeais[i]) {
                            solucoesIdeais[i] = matNormalizadaPesada[j][i];
                        }
                    }
                }
            }
        }
    }

    /**
     * Recebe a solução ideal de cada parametro e preenche um vetor com o somatório da distancia às mesmas.
     *
     * @param matNormalizadaPesada
     * @param solucoesIdeais
     * @param ditSolucaoIdeal
     */
    public static void vetorSeparacaodaSolucaoIdeal(double[][] matNormalizadaPesada, double[] solucoesIdeais, double[] ditSolucaoIdeal) {
        double soma = 0;
        for (int i = 0; i < matNormalizadaPesada.length; i++) {
            for (int j = 0; j < matNormalizadaPesada[0].length; j++) {
                soma = soma + Math.pow(matNormalizadaPesada[i][j] - solucoesIdeais[j], 2);
            }
            ditSolucaoIdeal[i] = Math.pow(soma, ((double) 1 / 2));
            soma = 0;
        }
    }

    /**
     * Recebe dois vetores com as distancias às soluções ideais (positivas e negativas) e devolve um vetor com a proximida relativa á solução ideal
     *
     * @param ditSolucaoIdeal
     * @param ditSolucaoIdealNegativa
     * @param solucoesMaisPertodaIdeal
     */
    public static void VetorSolucaoMaisPertodaIdeal(double[] ditSolucaoIdeal, double[] ditSolucaoIdealNegativa, double[] solucoesMaisPertodaIdeal) {
        for (int i = 0; i < ditSolucaoIdeal.length; i++) {
            solucoesMaisPertodaIdeal[i] = ((double) ditSolucaoIdealNegativa[i] / (ditSolucaoIdeal[i] + ditSolucaoIdealNegativa[i]));
        }
    }

    /**
     * Recebe um vetor e imprime a opcao correspondente ao maior numero
     *
     * @param vec
     * @param form
     */
    public static void printOpcaoAdequadaTOPSIS(double[] vec, String[] alternativas, Formatter form) {
        double max = 0;
        int opcao = -1;
        for (int i = 0; i < vec.length; i++) {
            if (vec[i] > max) {
                max = vec[i];
                opcao = i + 1;
            }
        }
        form.format("A opção adequada é a " + Math.round(opcao) + "ª (" + alternativas[opcao - 1] + ")");
        form.format("%n");
    }

    /**
     * Lê de ficheiro de texto a informação necessária para aplicar o método TOPSIS
     *
     * @param crt_beneficio
     * @param crt_custo
     * @param alternativas
     * @param pesos
     * @param mat
     * @param nomefich
     * @throws FileNotFoundException
     */
    public static void lerFicheiroTopsis(String[] crt_beneficio, String[] crt_custo, String[] alternativas, String[] criterios, double[] pesos, double mat[][], String nomefich, Formatter LogErros) throws FileNotFoundException {
        Scanner sc = new Scanner(new File(nomefich));
        double pesosTeste = 0, pesosTeste2 = 0;
        int l = 0;
        while (sc.hasNextLine()) {
            String linha = sc.nextLine();
            if (linha.length() > 0) {
                String linhaSemEspacosRepetidos = linha.trim().replaceAll("( )+", " ");
                String[] contLinha = linhaSemEspacosRepetidos.split(" ");
                if ((contLinha[0].equals("crt_beneficio") || contLinha[0].equals("md_crt_alt") || contLinha[0].equals("crt_custo") || contLinha[0].equals("vec_pesos") || contLinha[0].equals("crt") || contLinha[0].equals("alt") || contLinha[0].equals("1") || contLinha[0].equals("2") || contLinha[0].equals("3") || contLinha[0].equals("4") || contLinha[0].equals("5") || contLinha[0].equals("6") || contLinha[0].equals("7") || contLinha[0].equals("8") || contLinha[0].equals("9"))) {
                    if ((contLinha[0]).equals("crt_beneficio")) {
                        for (int i = 1; i < contLinha.length; i++) {
                            crt_beneficio[i - 1] = contLinha[i];
                        }
                    }
                    if ((contLinha[0]).equals("crt_custo")) {
                        for (int i = 1; i < contLinha.length; i++) {
                            crt_custo[i - 1] = contLinha[i];
                        }
                    }
                    if ((contLinha[0]).equals("vec_pesos")) {
                        String pesosT = (sc.nextLine().trim());
                        String pesosSplit[] = pesosT.split(" ");
                        if (pesosSplit.length == mat[0].length) {
                            for (int i = 0; i < pesos.length; i++) {
                                pesos[i] = Double.parseDouble(pesosSplit[i]);
                            }
                        } else {
                            registarErro(LogErros, pesosT, "O número de pesos no ficheiro não corresponde ao número de critérios!");
                            LogErros.close();
                            System.exit(1);
                        }

                        for (int i = 0; i < pesos.length; i++) {
                            pesosTeste = pesosTeste + pesos[i];
                        }
                        for (int i = 0; i < pesos.length; i++) {
                            if (pesos[i] == 0) {
                                pesosTeste2 = pesosTeste2 + 1;
                            }
                        }
                        if (pesosTeste != 1 || pesosTeste2 != 0) {

                            registarErro(LogErros, pesosT, "A soma dos critérios é diferente de 1 ou existem pesos iguais a 0!");
                            LogErros.close();
                            System.exit(1);
                        }
                    }
                    if (contLinha[0].equals("crt")) {
                        for (int i = 1; i < contLinha.length; i++) {
                            criterios[i - 1] = contLinha[i];
                        }
                    }
                    if (contLinha[0].equals("alt")) {
                        for (int i = 1; i < contLinha.length; i++) {
                            alternativas[i - 1] = contLinha[i];
                        }
                        while (sc.hasNextLine()) {
                            String numeros[] = sc.nextLine().split(" ");
                            for (int i = 0; i < numeros.length; i++) {
                                mat[l][i] = Double.parseDouble(numeros[i]);
                            }
                            l++;
                        }
                    }
                } else {
                    System.out.println("O Ficheiro de texto contém linhas inválidas!");
                    registarErro(LogErros, linhaSemEspacosRepetidos, "O Ficheiro de texto contém linhas inválidas!");
                    LogErros.close();
                    System.exit(1);
                }
            }
        }
        if (pesosTeste == 0) {
            System.out.println("Os pesos não foram introduzidos os mesmos vão ser divididos de forma igual!");
            for (int i = 0; i < pesos.length; i++) {
                pesos[i] = ((double) 1 / (pesos.length));
            }
        }
    }

    /**
     * Recebe um vetor de Strings com os critérios custo e preenche um vetor de strings com "-" se o correspondente á posição for custoo e "-" caso não o seja (caso não existam critérios custo todos serão beneficio).
     *
     * @param crt_beneficio
     * @param valorizacao
     */
    public static void converterMaisMenos(String[] crt_custo, String[] valorizacao) {
        for (int i = 0; i < crt_custo.length; i++) {
            if (crt_custo[i] == null) {
                valorizacao[(crt_custo.length) - 1 - i] = "+";
            } else {
                valorizacao[(crt_custo.length) - 1 - i] = "-";
            }
        }
    }
}
